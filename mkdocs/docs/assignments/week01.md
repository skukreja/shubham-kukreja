# 1. Principles and practices:

<b>I spent the entire week to understand problems around me at Vigyan Ashram. Challenges of rural India are distinctly visible at Pabal.I spoke to the native people of the village about the challenges they face and further discussed with the teachers at Vigyan Ashram.I came up with a lot of ideas which soon need a solution</b> 

##Project Ideas
<b>Idea 1- Pest Predictor:</b>A device which will <b>predict the probable pest on the crop for the farmers.</b>  
<b>Challenge:</b>The project depends on the <b>real data of pests on different crops</b> in various atmospheric conditions. It's difficult to fetch this information.


<b>Idea 2-Ammonia Buzzer:</b>A device to continuously <b>monitor the concentration of ammonia in water bodies</b> and alarm on increase in levels. This will keep the aquaponics garden healthy.  
<b>Challenge:The cost of the ammonia sensor is too high.</b>


<b>Idea 3- Crop Advisor:</b> A system which will <b>advice farmers of the appropriate crop to cultivate</b> in every season.  
<b>Challenge: A lot of variables</b> like weather forecast, market demands, personlised field of every farmer are involved in the project which makes it difficult to complete it in 6 months.


<b>Idea 4- Water Distillation Device:</b> To improve the design of an existing model of a distillation device of "Equitron" company.  
<b>Challenge:</b>It's an interesting project but I was less ineterested because the other problems if solved will be more benifitical to the society.

<b>Idea 5-Soil Mositure Sensor:</b> To make a <b>soil sensor which is sturdy and robust</b> with good life cycle when used in farms.  
<b>Challenge:</b> The project is based on <b>first principle of Science</b> and will involve a lot of experiementation in the lab.  


<b>Idea 6- Chemical Detector:</b>To <b>detect the amount of chemicals</b>(insectisides,pesticides) on fruits and vegeatbles.  
<b>Challenge:</b> The chemicals are in ppm level which makes it very difficult to detect and quantify.  

<b>Idea 7- Wild Boar Alert:</b> To use I.R sensors and inform farmers of wild boars or stop them from entering the farms in forest area.  
<b>Challenge:</b> I realised farmers are facing bigger problems which need to adressed before this challenge. 

<b>Idea 8- Water Requirement:</b> a device to intimdate the farmer of the requirement of water according to crop in the farm.  
<b>Challenge:</b>The optimum requirement will vary with every crop and land which makes it difficult to standardize. 

After brainstorming on all the project ideas, I fianlised to work on the idea 1-"Pest Predictor."

<b>"Pest Predictor"</b>

<b>1. Functionality:</b> The device will predict the probable pest on the crop for the farmers.The prediction will go as a text message on the farmer's mobile informing him to spray the appropriate pesticides/insectisides in optimum quantity.  
<b>2. Application:</b> The device will be mainly used by farmers and will be affordable. It will save their crop and money from buying excessive chemicals.  
<b>3. Sketch:</b>
![](../images/week1/1.png)
![](../images/week1/3.png)

<b>4. Training Dataset:The key component visible in the flow chart is "Training Dataset"i.e, the data which has information of atmospheric conditions in which pest is more susceptible to arrive on the crop. Yet no instution/organisation had this dataset.</b> This need of collecting data gave birth to the project "Weather Eye"

##<b> Final Project Idea-"Weather Eye"</b>
Weather eye will collect real-time temperature data and send the information on cloud. 

###<b> Sketch of "Weather Eye"</b>
<img src="../../images/week1/4.jpg">






###<b> Functionality and application of "Weather Eye"</b>

<b>Functioning:</b> Weather Eye will have digital thermometer as input device to sense tmeprature. The microcontroller board will be programmed to calculate humidity. These atmospheric conditions will be sent to the cloud by wifi module. 

<b>Application:</b> I plan to make the device in a clock shape so that it can be installed in open fields, large farms, closed rooms, polyhouse etc. Basically the device will be portable to install at any place where the temperature and humidity data has to be monitored. 




## Learning and Conslusions
"Every Challenge is an Opportunity".During the whole process of identifying and analyzing the problems, I have realised there are a lot of challenges in rural India. And all these problems can be given a shape of a project involving technology to adress them. 
